## PSR(PHP Standards Recommendations) ##
代码风格应该严格遵守PSR-1(基本编码标准)和PSR-2(代码风格指南)

- [PSR-1](http://www.php-fig.org/psr/psr-1/)
- [PSR-2](http://www.php-fig.org/psr/psr-2/)

[PSR中文参考](http://www.snblog.cn/archives/111)