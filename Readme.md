# 源码版本控制 #
- 使用 Git Flow 模型[http://blog.jobbole.com/76867/](http://blog.jobbole.com/76867/)
- 使用 Git Subtree 在项目之间共享代码 [http://aoxuis.me/post/2013-08-06-git-subtree](http://aoxuis.me/post/2013-08-06-git-subtree)
  - [用 Git Subtree 在多个 Git 项目间双向同步子项目，附简明使用手册](https://segmentfault.com/a/1190000003969060)
  - Git Submodule教程（部分旧项目使用了此工具，但在新的项目中将使用Subtree方案来替换） [https://yq.aliyun.com/articles/27002](https://yq.aliyun.com/articles/27002)

# 文档规范 #
Java API 接口文档使用 [Swagger](http://swagger.io) 注解后自动生成

# 运行环境管理 #
所有的后端工程，统一使用 [Docker](https://www.docker.com/) 作为环境管理工具，每一个工程都要在项目根目录下建立Docker镜像文件，
并在根目录建立Readme.md，说明建立项目运行容器的步骤、命令和注意事项。

# 编码标准 #

## 代码风格 ##

主要参考Google开源规范 [Google Style Guides](https://google.github.io/styleguide/)


- [HTML/CSS](code-style/HTMLnCSS.md)
- [Javascript](code-style/Javascript.md)
- [PHP](code-style/PHP.md) 
- [Java](code-style/Java.md)
- [Objective-c](code-style/Objective-c.md)

## 工程规范 ##


- Android
- IOS
- [API](engineering-specification/API/index.md)
- WebApp