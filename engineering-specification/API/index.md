# 项目业务逻辑复用：可复用组件库

```
所有项目的业务逻辑都要考虑复用，分模块封装到可复用组件库中，
不以具体项目为中心来进行开发，而是以可复用组件库为中心来进行开发。
```

- 当一个新项目来到时，先分析出项目所包含的业务逻辑，按模块划分好这些业务逻辑。
- 如果某些模块在可复用组件库已经有可复用的组件，那么直接把该组件引用到新项目。
- 如果某些模块在可复用组件库已经有可复用的组件，但又不完全满足项目需求，那么考虑升级或扩展该组件，再引用到新项目中。
- 如果新项目中的功能模块并没有可复用的组件可用，那么考虑规划新的可复用组件，来满足新项目的需求。


### 可复用组件库的实现

- 在java中，使用cn.springogic包来充当可复用组件库的角色。如cn.springlogic.user就是一个用户组件。
- 在PHP中，使用 [Apigility Logic](https://github.com/catworking/apigility-logic) 来充当可复用组件库的角色，可复用组件库的模块（组件）统一使用ApigilityXXX这样的命名空间。
  比如ApigiliityUser就是一个用户组件，它是一个PHP命名空间，同时是一个Apigility/ZF模块。


# 技术堆栈

## JAVA

### 使用Spring Boot作为应用程序框架
  - [Spring Boot 项目官网主页](http://projects.spring.io/spring-boot/)
  - [Spring Boot 文档中文翻译](https://github.com/qibaoguang/Spring-Boot-Reference-Guide)

### 使用Spring Security Oauth2搭建认证服务
  - [Spring Security Oauth2 项目官网主页](http://projects.spring.io/spring-security-oauth/)
  - [Spring Security 文档中文翻译](https://github.com/aquariuspj/spring-security-reference-cn)
  - [Spring Security Oauth2 官方开发者指南（英文原版）](http://projects.spring.io/spring-security-oauth/docs/oauth2.html)
  - [Spring Security Oauth2 官方开发者指南（中文版）](http://www.oschina.net/translate/oauth-2-developers-guide)
  - [Spring Boot 与 Oauth2 整合示例（官方）](https://spring.io/guides/tutorials/spring-boot-oauth2/)
  - Spring Security Oauth2 官方推荐的第三方讲解 PPT
    - [Spring Oauth2 数据建模与标识管理](https://speakerdeck.com/dsyer/data-modelling-and-identity-management-with-oauth2)
    - [Spring 中微服务的安全方案](https://speakerdeck.com/dsyer/security-for-microservices-with-spring)

### 使用Spring Data REST来实现RESTFul接口
  - [Spring Data REST 项目官网主页](http://docs.spring.io/spring-data/rest/docs/2.6.1.RELEASE/reference/html/)
  - [Spring Data REST入门中文参考资料](http://blog.csdn.net/soul_code/article/details/54108105)
  - [Spring Data JPA 项目官网主页](https://projects.spring.io/spring-data-jpa/)
  - [Spring Data JPA 文档中文翻译](https://github.com/ityouknow/spring-data-jpa-reference-documentation)
  - [Hibernate](http://hibernate.org/orm/documentation/getting-started/)
  - [Spring Data REST 如何（通过data-rest本身的功能）管理对象的关联关系](http://www.baeldung.com/spring-data-rest-relationships)
  - [Spring Data REST 如何解决JPA双向关联在生成Json时出现死循环的问题](http://blog.csdn.net/tomcat_2014/article/details/50624869)
  - [SPring Data REST 如何解决关联对象不能嵌套显示的问题（在Entity中把字段注解为不通过 Data-rest的HATEOAS方式暴露）](http://docs.spring.io/spring-data/rest/docs/current/reference/html/#projections-excerpts.projections)
### 开发原则
- 暂无
  
  

## PHP

### 框架
- 使用 [Zend Framework](https://framework.zend.com/) （PHP官方出品的类库&框架）技术体系
- 使用 [Apigility](https://apigility.org/) （PHP官方出品，基于ZF的Web Service快速开发框架）作为开发框架
- 统一使用 [Apigility 中的 RESTful 开发方式](https://apigility.org/documentation/intro/first-rest-service)来开发RESTFul接口，尽可能地杜绝RPC接口 
- 使用 [Apigility 集成的 Oauth2 模块](https://apigility.org/documentation/auth/authentication-oauth2)来实现安全认证。
- 使用 Apigility 自身的数据验证功能来[验证](https://apigility.org/documentation/content-validation/validating)&[过滤](https://apigility.org/documentation/content-validation/filtering)用户的输入。
- 使用 [Doctrine2.5+](http://doctrine-project.org/projects/orm.html) 来实现数据库层抽象，所用的数据操作都通过Doctrine来实现。
- 在使用 Doctrine 时，[使用“代码先行”的开发方式](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html)，也就是通过PHP类来定义数据库结构。
- 在使用 Doctrine 操作数据库时，尽可能地使用[QueryBuilder](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/query-builder.html)。

### 开发原则
- 模块与模块之间的通讯，通过EventManager提供的事件机制来实现，严禁直接调用对方的函数或方法。


# 杂项
- [理解 Oauth2.0 - 阮一峰](http://www.ruanyifeng.com/blog/2014/05/oauth_2_0.html)
- [JWT(Json Web Token)简介](http://www.haomou.net/2014/08/13/2014_web_token/)
- [Bearer Token 简介](https://blog.yorkxin.org/2013/09/30/oauth2-6-bearer-token)